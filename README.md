# Homebridge HTU21D Sensor Plugin

This is an [Homebridge](https://github.com/nfarina/homebridge) accessory plugin
for HTU21D temperature and humidity sensor connected via i2c.

## Install

The way in which I2C is configured varies from board to board. Sometimes no configuraton is required, but sometimes it is:
  - [Configuring I2C on the Raspberry Pi](https://github.com/fivdi/i2c-bus/blob/master/doc/raspberry-pi-i2c.md)
  - [Configuring Software I2C on the Raspberry Pi](https://github.com/fivdi/i2c-bus/blob/master/doc/raspberry-pi-software-i2c.md)
    - Consider software I2C when there are issues communicating with a device on a Raspberry Pi

```bash
npm install -g --unsafe-perm homebridge-htu21d-sensor
```

## Example config.json

```json
{
  "bridge": {
    "name": "Homebridge",
    "username": "CC:22:3D:E3:CE:30",
    "port": 51826,
    "pin": "031-45-154"
  },

  "description": "This is an example configuration file for HTU21D plugin.",

  "accessories": [
    {
      "accessory": "HTU21DSensor",
      "name": "HTU21D Sensor",
      "updateFrequency": 30
    }
  ],

  "platforms": [
  ]
}
```

### Optional Parameters
| Option | Default | Explanation |
| --- | --- | --- |
| `updateFrequency` | 1 | How frequently to read temperature and humidity from HTU21D (in seconds). |

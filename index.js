const Htu21d = require("./htu21d.js");

var Service, Characteristic;

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("environment", "HTU21DSensor", TemperatureSensor);
};

class TemperatureSensor {
  constructor(log, config) {
    this.state = true;
    this.log = log;
    this.config = config;
    this.name = config.name;
    this.currentTemperature = 20;
    this.currentHumidity = 0;
    this.sensor = new Htu21d(1, Number(config.updateFrequency || 1) * 1000);
    this.sensor.start()

    this.startReading = this.startReading.bind(this);
  }

  startReading() {
    this.sensor.on('readout-complete', (data) => {
      const { temperature, humidity } = data
      this.currentTemperature = temperature
      this.currentHumidity = humidity
      this.temperatureService.setCharacteristic(
        Characteristic.CurrentTemperature,
        temperature
      );
      this.humidityService.setCharacteristic(
        Characteristic.CurrentRelativeHumidity,
        humidity
      );
    });
  }

  getServices() {
    let informationService = new Service.AccessoryInformation();
    informationService
      .setCharacteristic(Characteristic.Manufacturer, "fungland")
      .setCharacteristic(Characteristic.Model, "HTU21D")
      .setCharacteristic(Characteristic.SerialNumber, "001-000-000");

    const temperatureService = new Service.TemperatureSensor(this.name);

    temperatureService
      .getCharacteristic(Characteristic.Name)
      .on("get", callback => {
        callback(null, this.name);
      });

    temperatureService
      .getCharacteristic(Characteristic.CurrentTemperature)
      .on("get", callback => callback(null, this.currentTemperature));

    const humidityService = new Service.HumiditySensor(this.name);

    humidityService
      .getCharacteristic(Characteristic.Name)
      .on("get", callback => {
        callback(null, this.name);
      });

    humidityService
      .getCharacteristic(Characteristic.CurrentRelativeHumidity)
      .on("get", callback => callback(null, this.currentHumidity));

    this.informationService = informationService;
    this.temperatureService = temperatureService;
    this.humidityService = humidityService;

    this.startReading();

    return [informationService, temperatureService, humidityService];
  }
}